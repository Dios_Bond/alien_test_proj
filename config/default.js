module.exports = {
    server: {
        port: 3000
    },
    tables: {
        tableName: 'books'
    },
    database: {
        master: {
            host: "localhost",
            user: "user",
            password: "password123",
            port: "3306",
            database: "books_db",
            connectionLimit: 5
        }
    }
}; 
