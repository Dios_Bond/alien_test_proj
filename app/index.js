const Koa = require('koa');
const conf = require('config');
const send = require('koa-send');
const Router = require('koa-router');
const KoaBody = require('koa-body');
require('koa-bodyparser'); 
const convert = require('koa-convert');
const catalog = './public';
const err = require('./router/error');
//const routerr = require ('./router/routes.js');
const books_func = require ('./controllers/func');


const app = new Koa();


let router = new Router(app);
let koaBody = convert(KoaBody());

    router
        //.all('/create', async (ctx, next) => {
        //  console.log('all req')
        //})
        .get('/', async (ctx, next) => {
            //ctx.body = await product.getAll()
            await send(ctx, ctx.path, { root: catalog + '/index.html' });
          console.log('App send page');
            console.log('get req')
        })
        .get('/books/:id', async (ctx, next) => {
            let result = await product.get(ctx.params.id);
            if (result) {
                ctx.body = result
            } else {
                ctx.status = 204;
            }
        })
  
        .post('/', async (ctx, next) => {
            //ctx.status = 200;
            console.log('post req');
            ctx.status = 204;
            await send(ctx, ctx.path, { root: catalog + '/index.html' });
        })
        .post('/create', koaBody, async (ctx, next) => {
            console.log('post create req');
            ctx.status = 204;
            console.log(await ctx.request.body)
            ctx.body = await books_func.create(ctx.request.body);
            await send(ctx, ctx.path, { root: catalog + '/index.html' });
        })
        .put('/books/:id', koaBody, async (ctx, next) => {
            ctx.status = 204;
            await product.update(ctx.params.id, ctx.request.body);
        });

    



        //app.use(routerr.routes());
      app.use(router.routes());
      app.use(router.allowedMethods());
    


app.listen(conf.server.port, function () {
    console.log('app start at port ', conf.server.port);
});



