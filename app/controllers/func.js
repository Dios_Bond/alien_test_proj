const query = require('mysql-query-promise');
const config = require('config');
const tableName = config.tables.tableName;

const controllers = {
    getAll: async () => {
        return query(`SELECT * from ${tableName}`);
    },
    get: async (id) => {
        let books = await query(`SELECT * FROM ${tableName} WHERE id=?`,[Number(id)]);
        return books[0];
    },
    create: async function ({ id, title, date, autor, deskription, image }) {
        let book = {
            title: String(title),
            date: Date(date),
            autor: String(autor),
            deskription: String(deskription),
            image: String(image)
        };
        if (id > 0) book.id = Number(id);
        let result = await query(`INSERT INTO ${tableName} SET ? ON DUPLICATE KEY UPDATE ?`,[book,book]);
        if (result.insertId) id = result.insertId;
        return crud.get(id);
    },
    update: async (id, book)=> {
        if (typeof book === 'object') {
            let updBook = {};
            if (book.hasOwnProperty('title')) updBook.title = String(book.title);
            if (book.hasOwnProperty('date')) updBook.date = Date(book.date);
            if (book.hasOwnProperty('autor')) updBook.autor = String(book.autor);
            if (book.hasOwnProperty('deskription')) updBook.deskription = String(book.deskription);
            if (book.hasOwnProperty('image')) updBook.image = String(book.image);
            let result = await query(`UPDATE ${tableName} SET ? WHERE id=?`,[updBook, Number(id)]);
            return result.affectedRows;
        }
    }
};
module.exports = controllers;
